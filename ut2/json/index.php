<?php 
    /**
     * Modelo
     */
    function getData($fichero) {
        $datosJSON = file_get_contents($fichero);
        $datosArray = json_decode($datosJSON, true);
        return $datosArray;
    }
?>

<?php 
    function generateSelect($array, $name, $description, $selected=""){
        $html = '';
        $html = "<select name ='$name'>";
        echo "<option value=''>Seleccione $name</option>";
        foreach ($array as $element) {
            $nombreID = $name."_id";
            $select = $selected == $element[$nombreID] ? "selected" : "";
            $html.= "<option value='$element[$nombreID]' selected>".$element["nombre"]."</option>"."<br>";
        }
        $html.= "</select>";
        return $html;
    }
?>

<?php
    /**
     * Controlador
     */
    $rutaProvincias = "provincias.json";
    $provinciasArray = getData($rutaProvincias);
    $description = 'no se';
    echo generateSelect($provinciasArray, 'provincia',$description);
   

    $rutaMunicipios = "municipios.json";
    $municipiosArray = getData($rutaMunicipios);
    echo generateSelect($municipiosArray, 'municipio',$description);
?>