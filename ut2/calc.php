<?php
    // COGER PARAMETROS.
    $op1 = 0;
    $op2 = 0;
    $res = 0;
    $mensaje = "";
    
    // Preguntar por la operacion.
    if (isset($_REQUEST["operacion"])) {
        $op1 = $_REQUEST["op1"];
        $op2 = $_REQUEST["op2"];
        $operacion = $_REQUEST["operacion"];

        switch ($operacion) {
            case "+" : $res = $op1 + $op2; break;
            case "-" : $res = $op1 - $op2; break;
            case "*" : $res = $op1 * $op2; break;
            case "/" : $res = $op1 / $op2; break;
            case "**" : $res = $op1 ** $op2; break;
            default: 
                $mensaje = "operación no definidad.";
        }
    }
?>

<?php
    include "calc_vista.php";
?>