<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
  <meta name="description" content="Calculadora">
  <meta name="keywords" content="HTML, CSS, JavaScript">
  <meta name="author" content="Jose Jesús">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">    <title>Calc</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Orbitron&display=swap" rel="stylesheet">    

    <style>
        body {
        }

        input {
            font-family: 'Orbitron', sans-serif;
            text-align: right;
        }

        #res {
            background-color: yellow;
        }

        button {
            width: 100%;
            font-size:20px;
        }

        #del {
            background-color: #E6E6FA;
        }

        .container {
            text-align: center;
            background: white;
        }

        table {
            margin: 0 auto;
        }



    </style>

</head>
<body>

<div class="container">
<h3>CALCULADORA</h3>

<form action="calc.php" method="GET">
<table>
    <tr>
        <td colspan="3">
            <input name="op1" value="<?= $op1 ?>" type="text">
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <input name="op2" value="<?= $op2 ?>" type="text">
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <input id="res" name="res" value="<?= $res ?>" type="text" readonly>
        </td>
    </tr>

    <tr>
        <td><button type="submit"  name="operacion" value="+">+</button></td>
        <td><button type="submit"  name="operacion" value="-">-</button></td>
        <td><button type="submit" >x</button></td>
    </tr>

    <tr>
        <td><button type="submit" name="operacion" value="*">x</button></td>
        <td><button type="submit" name="operacion" value="/">/</button></td>
        <td><button type="submit" name="operacion" value="**">pov</button></td>
    </tr>

    <tr>
        <td><button type="submit" name="operacion" value="">+</button></td>
        <td><button type="submit" name="operacion" value="">-</button></td>
        <td><button type="button" onclick="location.href='calc.php';">DEL</button></td>
    </tr>


</table>
</form>

</div>

</body>
</html>