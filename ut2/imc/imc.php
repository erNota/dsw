<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- 
            Autor: Agustín Domingo Déniz Quintana
            Ejercicio: IMC
        -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="themes/health.jpg">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Noto+Serif:ital@1&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <title>IMC</title>
    </head>
    <body>
        <header class="container-lg">
        <h1>IMC</h1>
        El Índice de Masa Corporal (IMC) es una medida de asociación entre el peso y la talla de una persona. El IMC es usado como uno de varios indicadores para evaluar el estado nutricional.
        </header>
        
        <?php 
            $peso = 0;
            $altura = 0;
            $imc = 0;
            $descripcion = "";
        ?>

        <form method="POST">
            <div class="container">
                <div>
                    <label for="peso">Peso: </label>
                    <input type="text" class= "form-control-lg" name="peso" value= "<?= $peso ?>" />
                </div>
                <div>
                    <label for="altura">Altura: </label>
                    <input type="text" class= "form-control-lg" name="altura" value= "<?= $altura ?>" />
                </div>

                <?php

                    if ( isset($_REQUEST["peso"]) && isset($_REQUEST["altura"])){
                        //script para calcular el IMC
                        $peso = $_REQUEST["peso"];
                        $altura = $_REQUEST["altura"];

                        if( empty($_REQUEST["peso"]) && empty($_REQUEST["altura"])){
                            $peso = 0;
                            $altura = 0;
                        }else{
                            $imc = round ($peso/($altura*$altura),3);

                            if( $imc <= 18.5){
                                $descripcion="Bajo peso";
                            }else if($imc <=24.9){
                                $descripcion = "Peso nomal";
                            }else if($imc < 29.9){
                                $descripcion = "Sobrepeso";
                            }else{
                                $descripcion = "Obesidad";
                            }
                        }  
                    }
                ?>
                <div>
                    <label for="resultado">Resultado: </label>
                    <input type="text" class= "form-control-lg"name="resultado" value= "<?= $imc ?>" />
                    <input type="text" class= "form-control-lg" name="descripcion" value= "<?= $descripcion ?>" />
                </div>
                <div>
                    <button type= "submit" class= "btn boton">Calcular</button>
                    <button type="button" onclick="location.href='./imc.php'"  class="btn boton">Reset</button>
                </div>
                
                </div>
            </form>
    </body>
</html>