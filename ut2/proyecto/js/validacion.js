$(document).ready(function () {

    // * Patrones genericos para los string, enteros y email

    // * Patron string
    $.validator.addMethod("patronString", function (value, element) {
        var pattern = /^([a-zA-Z])+/;
        return this.optional(element) || pattern.test(value);
    }, "No se admiten enteros");

    // * Patron email
    $.validator.addMethod("patronEmail", function (value, element) {
        var pattern = /^([a-z0-9_.-])+@([a-z0-9_.-])+\.([a-z])+/;
        // var pattern = /^([a-zA-Z0-9!#$%&'*_+-]([\.]?[a-zA-Z0-9!#$%&'*_+-])+@[a-zA-Z0-9]([^@&%$\/()=?¿!.,:;]|\d)+[a-zA-Z0-9][\.][a-zA-Z]{2,4}([\.][a-zA-Z]{2})?)+/;
        return this.optional(element) || pattern.test(value);
    }, "Ej: ejemplo@ejemplo.es");

    // * Patron entero
    $.validator.addMethod("patronEntero", function (value, element) {
        var pattern = /^([0-9])+/;
        return this.optional(element) || pattern.test(value);
    }, "No se adminten String ej: 12345");

    // * Aplicamos las restricciones especificas para cada elemento donde
    // el required establece los compos obligatorios, max y min es para la longitud y por ejemplo:
    // patronString hace la llamada de su funcion para aplicar las condiciones genericas de los strings

    $("#formulario").validate({
        // ! Restricciones de los campos.
        rules: {
            // # DATOS DEL REPRESENTANTE ###################################################################################
            documento: {
                required: true
            },

            numIdentificacion: {
                required: true,
                minlength: 9,
                patronNumIdentificacion: true
            },

            nombre: {
                required: true,
                minlength: 3,
                maxlength: 8,
                patronString: true
            },

            primerApellido: {
                required: true,
                minlength: 3,
                maxlength: 8,
                patronString: true
            },

            segundoApellido: {
                required: true,
                minlength: 3,
                maxlength: 8,
                patronString: true
            },

            calidad: {
                required: true
            },

            telefono: {
                minlength: 9,
                maxlength: 9,
                number: true,
                patronEntero: true
            },

            movil: {
                minlength: 9,
                maxlength: 9,
                number: true,
                patronEntero: true
            },

            correo: {
                required: true,
                patronEmail: true
            },

            fecha: {
                required: true
            },

            // # DOMICILIO DE CONTACTO ###################################################################################
            tipoVia: {
                required: true
            },

            nombreVia: {
                required: true,
                minlength: 4,
                maxlength: 20,
                patronString: true
            },

            numero: {
                required: true,
                minlength: 1,
                maxlength: 2,
                number: true,
                patronEntero: true
            },

            bloque: {
                required: true,
                minlength: 1,
                maxlength: 1,
                number: true,
                patronEntero: true
            },

            escaleras: {
                number: true,
                minlength: 1,
                maxlength: 1,
                patronEntero: true
            },

            piso: {
                number: true,
                minlength: 1,
                maxlength: 1,
                patronEntero: true
            },

            portal: {
                number: true,
                minlength: 1,
                maxlength: 1,
                patronEntero: true
            },

            letra: {
                minlength: 1,
                maxlength: 1
            },

            puerta: {
                number: true,
                minlength: 1,
                maxlength: 2,
                patronEntero: true
            },

            // complemento: {

            // },

            pais: {
                required: true
            },

            provincia: {
                required: true
            },

            municipio: {
                required: true
            },

            isla: {
                required: true
            },

            localidad: {
                required: true
            },

            codigoPostal: {
                number: true,
                minlength: 5,
                maxlength: 5,
                patronEntero: true
            },

            // # ALERGIAS, PATOLOGIAS O DIETAS ESPECIALES ###################################################################################
            textarea: {
                maxlength: 500
            },

            // # Datos academicos
            itinerario: {
                required: true
            },

            lenguaOp1: {
                required: true
            },

            opcionales1: {
                required: true
            },

            opcionales2: {
                required: true
            },

            opcionales3: {
                required: true
            }
        },

        // ! Definición de los mensajes de error.
        messages: {
            // # DATOS DEL REPRESENTANTE ###################################################################################
            documento: {
                required: 'El campo es requerido'
            },

            numIdentificacion: {
                required: 'El campo es requerido',
                minlength: 'La longitud minima es de 9 caracteres'
            },

            nombre: {
                required: 'No se admiten enteros ej: Jose',
                minlength: 'La longitud minina es de 3 caracteres',
                maxlength: 'La longitud maxima es de 8 caracteres'
            },

            primerApellido: {
                required: 'El campo es requerido ej: Navarro',
                minlength: 'La longitud minima es de 3 caracteres',
                maxlength: 'La longitud maxima es de 8 caracteres'
            },

            segundoApellido: {
                required: 'El campo es requerido ej: Cabrera',
                minlength: 'La longitud minima es de 3 caracteres',
                maxlength: 'La longitud maxima es de 8 caracteres'
            },

            calidad: {
                required: 'El campo es requerido'
            },

            telefono: {
                minlength: 'La longitud minima es de 9 caracteres',
                maxlength: 'La longitud maxima es de 9 caracteres',
                number: 'Solo números enteros ej: 123 45 67 89'
            },

            movil: {
                required: 'El campo es requerido ej: 12345',
                minlength: 'La longitud minima es de 9 caracteres',
                maxlength: 'La longitud maxima es de 9 caracteres',
                number: 'Solo números enteros ej: 123 45 67 89'
            },

            correo: {
                required: 'El campo es requerido',
                email: 'Ej: ejemplo@ejemplo'
            },

            fecha: {
                required: 'El campo es requerido',
            },

            // # DOMICILIO DE CONTACTO ###################################################################################
            tipoVia: {
                required: 'El campo es requerido'
            },

            nombreVia: {
                required: 'El campo es requerido ej: Camino Pintor Felo Monzón',
                minlength: 'La longitud minima es de 9 caracteres',
                maxlength: 'La longitud maxima es de 20 caracteres',
                patronString: 'No admiten enteros ej: Camino Pintor Felo Monzón'
            },

            numero: {
                required: 'El campo es requerido ej: 1',
                minlength: 'La longitud minima es de 2 caracteres',
                maxlength: 'La longitud maxima es de 2 caracteres',
                patronEntero: 'Solo numeros enteros ej: 1'
            },

            bloque: {
                required: 'El campo es requerido ej: 1',
                minlength: 'La longitud minima es de 1 caracteres',
                maxlength: 'La longitud maxima es de 1 caracteres',
                patronEntero: 'Solo numeros enteros ej: 1'
            },

            escaleras: {
                minlength: 'La longitud minima es de 1 caracteres',
                maxlength: 'La longitud maxima es de 1 caracteres',
                patronEntero: 'Solo numeros enteros ej: 1'
            },

            piso: {
                minlength: 'La longitud minima es de 1 caracteres',
                maxlength: 'La longitud maxima es de 1 caracteres',
                patronEntero: 'Solo numeros enteros ej: 1'
            },

            portal: {
                minlength: 'La longitud minima es de 1 caracteres',
                maxlength: 'La longitud maxima es de 1 caracteres',
                patronEntero: 'Solo numeros enteros ej: 1'
            },

            letra: {
                minlength: 'La longitud minima es de 1 caracteres',
                maxlength: 'La longitud maxima es de 1 caracteres',
                patronString: 'No se admiten enteros ej: A'
            },

            puerta: {
                minlength: 'La longitud minima es de 1 caracteres',
                maxlength: 'La longitud maxima es de 2 caracteres',
                patronEntero: 'Solo numeros enteros ej: 1'
            },

            // complemento: {
            //     patronEntero: 'Solo numeros enteros'
            // },

            pais: {
                required: 'El campo es requerido'
            },

            provincia: {
                required: 'El campo es requerido'
            },

            municipio: {
                required: 'El campo es requerido'
            },

            isla: {
                required: 'El campo es requerido'
            },

            localidad: {
                required: 'El campo es requerido'
            },

            codigoPostal: {
                required: 'El campo es requerido ej: 35 300',
                minlength: 'La longitud minima es de 5 caracteres',
                maxlength: 'La longitud maxima es de 5 caracteres',
                patronEntero: 'Solo numeros enteros ej: 35 300'
            },

            // # ALERGIAS, PATOLOGIAS O DIETAS ESPECIALES ###################################################################################
            textarea: {
                maxlength: 'La longitud maxima es de 500 caracteres'
            },

            //Datos academicos
            itinerario: {
                required: 'Debes seleccionar 1 itinerario',
            },

            lenguaOp1: {
                required: 'Debes seleccionar una lengua-extranjera opcional',
            },

            opcionales1: {
                required: 'Debes seleccionar una opcional del bloque 1',
            },

            opcionales2: {
                required: 'Debes seleccionar una opcional del bloque 2'
            },

            opcionales3: {
                required: 'Debes seleccionar una opcional del bloque 3'
            }
        }
    });
});