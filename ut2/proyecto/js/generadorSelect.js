$(document).ready(function() {

    $.getJSON("json/provincias.json", function(provincias) {

        $.each(provincias, function(indice, dato) {
            $('#provincia').append($('<option></option>').attr('value', dato.provincia_id).text(dato.nombre));
        })

    });

    $.getJSON("json/municipios.json", function(data) {
        municipios = data;
    });

    $("#provincia").change(function() {
        $('#municipio').empty();

        $.each(municipios, function(indice, dato) {
            if ($("#provincia").val() == dato.provincia_id) {
                $('#municipio').append($('<option></option>').attr('value', dato.municipio_id).text(dato.nombre));
            }
        })
    });

    $.getJSON("json/islas.json", function(data) {
        islas = data;
    });

    $("#provincia").change(function() {
        $('#isla').empty();

        $.each(islas, function(indice, dato) {
            if ($("#provincia").val() == dato.provincia_id) {
                $('#isla').append($('<option></option>').attr('value', dato.isla_id).text(dato.nombre));
            }
        })
    })
});