<?php
    // include "datos/datos.php";
    // * Filtrado generico

    function filtrado($dato)
    {
        $dato = trim($dato);
        $dato = stripslashes($dato);
        $dato = htmlspecialchars($dato);
        return $dato;
    }

    // * Funciones valores Obligatorios
    function validarStringsOblig($datos, $value, $patron, $min, $max, $mensaje){
        if(empty($_POST[$value])){
            $datos["error"][$value] = "Campo vacio ".$value."<br/>";
        }else{
            $recibir = filtrado($_POST[$value]);
            if (preg_match($patron, $recibir)) {
                if ((strlen($recibir) >= $min) && (strlen($recibir) <= $max)) {
                    $datos["datos"][$value] = $recibir;
                }else{
                    $datos["error"][$value] = "Se debe respetar la longitud ".$value."<br/>";
                }
            }else{
                $datos["error"][$value] = $mensaje."<br/>";
            }
        }
        return $datos;
    }

    function validarEnterosOblig($datos, $value, $patron, $min, $max, $mensaje){
        if(empty($_POST[$value])){
            $datos["error"][$value] = "Si introduces solamente un 0 como numero de telefono se considera vacio".$value."<br/>";
        }else{
            $recibir = filtrado($_POST[$value]);
            if (preg_match($patron, $recibir)) {
                if ((strlen($recibir) >= $min) && (strlen($recibir) <= $max)) {
                    $datos["datos"][$value] = $recibir;
                }else{
                    $datos["error"][$value] = "Se debe respetar la longitud ".$value."<br/>";
                }
            }else{
                $datos["error"][$value] =$mensaje."<br/>";
            }
        }
        return $datos;
    }

    function validarSelectsOblig($datos, $value){
        if(empty($_POST[$value])){
            $datos["error"][$value] ="Seleccione ".$value."<br/>";
        }else{
            $recibir = filtrado($_POST[$value]);
            $datos["datos"][$value] = $recibir;
        }
        return $datos;
    }

    function validarOtrosOblig($datos, $value, $patron, $mensaje){
        if(empty($_POST[$value])){
            $datos["error"][$value] ="Campo vacio ".$value."<br/>";
        }else{
            $recibir = filtrado($_POST[$value]);
            if (preg_match($patron, $recibir)) {
                $datos["datos"][$value] = $recibir;
            }else{
                $datos["error"][$value] =$mensaje."<br/>";
            }
        }
        return $datos;
    }

    // * Funciones valores no Obligatorios
    function validarStringsNOblig($datos, $value, $patron, $min, $max){
        $recibir = filtrado($_POST[$value]);
        if (preg_match($patron, $recibir)) {
            if (strlen($recibir) >= $min && strlen($recibir) <= $max) {
                $datos["datos"][$value] = $recibir;
            }else{
                $datos["error"][$value] ="Se debe respetar la longitud ".$value."<br/>";
            }
        }
        return $datos;
    }

    function validarEnterosNOblig($datos, $value, $patron, $min, $max){
        $recibir = filtrado($_POST[$value]);
        if (preg_match($patron, $recibir)) {
            if ((strlen($recibir) >= $min) && (strlen($recibir) <= $max)) {
                $datos["datos"][$value] = $recibir;
            }else{
                $datos["error"][$value] ="Se debe respetar la longitud ".$value."<br/>";
            }
        }
        return $datos;
    }

    function subirArchivo($archivo, $datos){
        //Recogemos el archivo enviado por el formulario
        $arch = $archivo['name'];
        //Si el archivo contiene algo y es diferente de vacio
        if (isset($arch) && $arch != "") {
            //Obtenemos algunos datos necesarios sobre el archivo
            $tipo = $archivo['type'];
            $tamano = $archivo['size'];
            $temp = $archivo['tmp_name'];
            //Se comprueba si el archivo a cargar es correcto observando su extensión y tamaño
            if (!((strpos($tipo, "pdf") || strpos($tipo, "txt") || strpos($tipo, "jpeg") || strpos($tipo, "jpg") || strpos($tipo, "png") || strpos($tipo, "doc") || strpos($tipo, "docx")) && ($tamano < 2000000))) {
                $datos["error"] ='<div><b>Error. La extensión o el tamaño de los archivos no es correcta.<br/>
                - Se permiten archivos .pdf, .txt, .jpg, .jpeg, .png, .doc, .docx y de 2Mb como máximo.</b></div>';
            }
            else {
                //Si la imagen es correcta en tamaño y tipo
                //Se intenta subir al servidor
                if (move_uploaded_file($temp, "imagenes/".$datos["datos"]["numIdentificacion"].'_'.$arch)) {
                    //Mostramos el mensaje de que se ha subido co éxito
                    $datos["error"] ='<div><b>Se ha subido correctamente la imagen.</b></div>';
                    //Mostramos la imagen subida
                    $datos["error"] ='<p><img src="imagenes/'.$arch.'"></p>';
                }
                else {
                    //Si no se ha podido subir la imagen, mostramos un mensaje de error
                    $datos["error"] ='<div><b>Ocurrió algún error al subir el fichero. No pudo guardarse.</b></div>';
                }
            }
        }
    }

    function generarSolicitud($datos){
        // var_dump($datos);
        $json = json_encode($datos);
        $file = 'json/solicitudes.json';
        file_put_contents($file, $json);
        echo '<div class="correcto">';
        echo "Se ha presentado la solicitud correctamente";
        echo "</div>";
    }

?>