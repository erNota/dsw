<?php
//* Array ASOCIATIVA
$datos = [
    "error" => [],

    "datos" => [
        // Datos representante
        "representante" => "",
        "documento" => "",
        "numIdentificacion" => "",
        "nombre" => "",
        "primerApellido" => "",
        "segundoApellido" => "",
        "calidad" => "",
        "movil" => "",
        "correo" => "",

        // Datos de contacto
        "tipoVia" => "",
        "nombreVia" => "",
        "numero" => "",
        "pais" => "",
        "provincia" => "",
        "isla" => "",
        "municipio" => "",
        "localidad" => "",

        // Datos representante
        "telefono" => "",
        "fecha" => "",

        // Datos de contacto
        "bloque" => "",
        "escalera" => "",
        "piso" => "",
        "portal" => "",
        "letra" => "",
        "puerta" => "",
        "cp" => "",

        // Mas datos
        "masDatos" => "",

        // Alergias...
        "textarea" => "",

        // Datos academicos
        "itinerario" => "",
        "lengua" => "",
        "filosofia" => "",
        "edFisica" => "",
        "matematicas" => "",
        "fisicayquimica" => "",
        "lenguaOp1" => "",
        "opcionales1" => "",
        "opcionales2" => "",
        "opcionales3" => "",

        // Consentimientos
        "consentimiento1" => "",
        "consentimiento2" => "",
        "consentimiento3" => "",
        "consentimiewnto4" => "",
    ]
];
