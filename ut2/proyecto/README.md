###### Tomás Navarro y Agustín Déniz
# PROYECTO DSW

El proyecto consistia en la creacion de un formulario que cumpliera con estos requisitos:

1. El usuario debe introducir datos obligatorios marcados en rojo.
2. No hace falta que el diseño sea igual.
3. Cuando se pulse el botón guardar se debe verificar que los datos introducidos son
correctos, del tipo esperado (email, fecha, numero, etc), no están vacios y limpios de
posibles códigos de html, php, etc. En caso de errores debemos mostrar los datos
introducidos previamente para corregirlos. En caso de estar correctos todos los datos
se debe mostrar un mensaje de “Su solicitud será procesada. En breve nos pondremos
en contacto con uds para facilitarle más información”.
4. La validación en el cliente es opcional pero se tendrá en cuenta.
5. Pongan nombre a los campos con sentido común: sin tildes, ni espacios en blanco y que indiquen la información.
6. Una vez introducidos los datos y verificado que están correctos, se deben almacenar todos los datos en el fichero datos.json.
7. Los ficheros se subirán en la carpeta documentos dentro del proyecto con el nombre del fichero, anteponiendo el nif del solicitante controlando las extensiones indicadas y el tamaño. Ejemplo:
    a. “100000z-documento1.png”
    b. “100000z-documento2.pdf”

### Explicación

Usamos librerias de ***JQuery*** para la creacion de la validacion en cliente asi como la auto creacion de los selects por medio de ficheros .json, que relacionaran los selects de "*Provincia*", "*Municipio*" y "*Isla*".

Marcamos con asteriscos (*) los datos obligatorios, que a travez de las reglas marcadas de JQuery mandaremos mensajes automaticos para ayudar al usuario a rellenar el formulario.

Para la validacion de servidor creamos varias funciones para la reutilisacion de codigo, asi como un array asociativo  que guarda temporalmente los valores para su posterior validacion asi como los errores que puedan ir surguiendo para luego mostrarlos. (***datos/datos.json***)

Todos los mensajes de error se mostraran arriba en un recuadro rojo facil de identificar. Despues de la validacion de todos los datos se guardaran un un archivo. (***json/solicitudes.json***)

Para los estilos se nos ocurrio usar el preprocesador ***SASS*** ya que hicimos cursos en OpenWebinars sobre él, sin embargo al final nos vimos apurados y cambiamos de idea.

En nuestro caso cumplimos con bastantes requisitos del proyecto, sin embargo por falta de conocimientos nos hemos visto abordados en algunos problemas, pero a base de ensayo error y a la documentacion de la red hemos solventados muchos problemas.

El proyecto lo tenemos subido a GitLab junto a las demas practicas del modulo la ruta exacta es (dsw/ut2/proyecto) [GitLab](https://gitlab.com/erNota/dsw)

###Problemas
Para la parte de auto creacion de los selects, no conseguimos que nos funcionara adecuadamente ya que al marcar en municipio "Las palmas de Gran canaria" no se auto seleccionaba "Gran Canaria" sino que aparecian varias opciones.

A la hora de hacer las validaciones a travez de expresiones regulares vimos que algunas no nos funcionaban y perdimos bastante tiempo encontrando el porque ocurria.
