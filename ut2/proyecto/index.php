<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- Librerias del componente -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
        <script src="js/validacion.js"></script>

        <script src="js/generadorSelect.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <link rel="icon" href="themes/gobCanarias.jpg" type="image/x-icon">

        <title>Formulario</title>
    </head>

    <body>
        <div class="container_all">
            <h1>Solicitud de Servicios</h1>
            <form enctype="multipart/form-data" action="<?php $_SERVER['PHP_SELF']; ?>" name="formulario" id="formulario" method="POST">
                <!-- <div class="errores"> -->
                    <?php
                        include "controlador.php";
                    ?>
                <!-- </div> -->

                <!-- // * DATOS ACTÚA COMO REPRESENTANTE -->
                <span class="separadores">DATOS ACTÚA COMO REPRESENTANTE</span>
                <div class="container_representante">
                    <label for="representante">¿ Actúa como representante ?</label>
                    <br />
                    <input type="radio" name="representante" id="alumno" value="Alumno">
                    <label for="representante">Alumno/a</label>
                    <input type="radio" name="representante" id="representante" value="Representante">
                    <label for="representante">Representante</label>
                </div>

                <!-- // # DATOS DEL REPRESENTANTE ################################################################################### -->
                <span class="separadores">DATOS DEL REPRESENTANTE</span>
                <div class="container_datosRepresentante">
                    <div class="tipoDocumento">
                        <label for="">Tipo de documento (*)</label>
                        <select name="documento" id="documento" value='<?= isset($_POST["documento"])?$_POST["documento"]:""; ?>' required>
                            <!-- <option value=""> -- Seleccione -- </option> -->
                            <option selected="true" value="NIF">NIF</option>
                            <option value="NIE">NIE</option>
                        </select>
                    </div>

                    <div class="numIdentificacion">
                        <label for="numIdentificacion">Nº de identificacion: (*)</label>
                        <input type="text" name="numIdentificacion" id="numIdentificacion" value='<?= isset($_POST["numIdentificacion"])?$_POST["numIdentificacion"]:""; ?>' required/>
                    </div>

                    <div class="nombre">
                        <label for="nombre">Nombre: (*)</label>
                        <input type="text" name="nombre" id="nombre" value='<?= isset($_POST["nombre"])?$_POST["nombre"]:""; ?>'/>
                    </div>

                    <div class="primerApellido">
                        <label for="primerApellido">Primer apellido: (*)</label>
                        <input type="text" name="primerApellido" id="primerApellido" value='<?= isset($_POST["primerApellido"])?$_POST["primerApellido"]:""; ?>' required />
                    </div>

                    <div class="segundoApellido">
                        <label for="segundoApellido">Segundo apellido: (*)</label>
                        <input type="text" name="segundoApellido" id="segundoApellido" value='<?= isset($_POST["segundoApellido"])?$_POST["segundoApellido"]:""; ?>' required />
                    </div>

                    <div class="calidad">
                        <label for="calidad">En calidad de: (*)</label>
                        <select name="calidad" id="calidad" required>
                            <option selected="true" value="Padres/Madre">Padre/Madre</option>
                            <option value="Tutor">Tutor</option>
                        </select>
                    </div>

                    <div class="telefono">
                        <label for="telefono">Teléfono fijo:</label>
                        <input type="number" name="telefono" id="telefono" value='<?= isset($_POST["telefono"])?$_POST["telefono"]:""; ?>'/>
                    </div>

                    <div class="movil">
                        <label for="movil">Teléfono movil: (*)</label>
                        <input type="number" name="movil" id="movil" value='<?= isset($_POST["movil"])?$_POST["movil"]:""; ?>' required />
                    </div>

                    <div class="correo">
                        <label for="correo">Correo electrónico: (*)</label>
                        <input type="text" name="correo" id="correo" value='<?= isset($_POST["correo"])?$_POST["correo"]:""; ?>' required />
                    </div>

                    <div class="fecha">
                        <label for="fecha">Fecha Nacimiento:</label>
                        <input type="date" name="fecha" id="fecha" />
                    </div>
                </div>

                <!-- // # DOMICILIO DE CONTACTO ################################################################################### -->
                <span class="separadores">DOMICILIO DE CONTACTO</span>
                <div class="container_contacto">
                    <div class="tipoVia">
                        <label for="tipoVia">Tipo de vía: (*)</label>
                        <select name="tipoVia" id="tipoVia" required>
                            <option value="Avenida">Avenida</option>
                            <option selected="true" value="Calle">Calle</option>
                            <option value="Callejón">Callejón</option>
                            <option value="Camino">Camino</option>
                            <option value="Carretera">Carretera</option>
                            <option value="Glorieta">Glorieta</option>
                            <option value="Pasaje">Pasaje</option>
                            <option value="Paseo">Paseo</option>
                            <option value="Plaza">Plaza</option>
                            <option value="Polígono">Polígono</option>
                            <option value="Rambla">Rambla</option>
                            <option value="Residencia">Residencia</option>
                            <option value="Ronda">Ronda</option>
                            <option value="travesía">Travesía</option>
                            <option value="Urbanización">Urbanización</option>
                            <option value="Vía">Vía</option>
                        </select>
                    </div>

                    <div class="nombreVia">
                        <label for="nombreVia">Nombre de vía: (*)</label>
                        <input type="text" name="nombreVia" id="nombreVia" value='<?= isset($_POST["nombreVia"])?$_POST["nombreVia"]:""; ?>' required />
                    </div>

                    <div class="numero">
                        <label for="numero">Número: (*)</label>
                        <input type="number" name="numero" id="numero" value='<?= isset($_POST["numero"])?$_POST["numero"]:""; ?>' required />
                    </div>

                    <div class="bloque">
                        <label for="bloque">Bloque:</label>
                        <input type="number" name="bloque" id="bloque" value='<?= isset($_POST["bloque"])?$_POST["bloque"]:""; ?>' />
                    </div>

                    <div class="escalera">
                        <label for="escalera">Escalera:</label>
                        <input type="number" name="escalera" id="escalera" value='<?= isset($_POST["escalera"])?$_POST["escalera"]:""; ?>'/>
                    </div>

                    <div class="piso">
                        <label for="piso">Piso:</label>
                        <input type="number" name="piso" id="piso" value='<?= isset($_POST["piso"])?$_POST["piso"]:""; ?>' />
                    </div>

                    <div class="portal">
                        <label for="portal">Portal:</label>
                        <input type="number" name="portal" id="portal" value='<?= isset($_POST["portal"])?$_POST["portal"]:""; ?>'/>
                    </div>

                    <div class="letra">
                        <label for="letra">Letra:</label>
                        <input type="text" name="letra" id="letra" value='<?= isset($_POST["letra"])?$_POST["letra"]:""; ?>' />
                    </div>

                    <div class="puerta">
                        <label for="puerta">Puerta:</label>
                        <input type="number" name="puerta" id="puerta" value='<?= isset($_POST["puerta"])?$_POST["puerta"]:""; ?>'/>
                    </div>

                    <div class="pais">
                        <label for="pais">País: (*)</label>
                        <select name="pais" id="pais" required>
                            <option selected="true" value="España">España</option>
                        </select>
                    </div>

                    <div class="provincia">
                        <label for="provincia">Provincia: (*)</label>
                        <select name="provincia" id="provincia" required>
                        </select>
                    </div>

                    <div class="municipio">
                        <label for="municipio">Municipio: (*)</label>
                        <select name="municipio" id="municipio" required>
                        </select>
                    </div>

                    <div class="isla">
                        <label for="isla">Isla: (*)</label>
                        <select name="isla" id="isla" required>
                            <option value="Gran Canaria">Gran Canaria</option>
                        </select>
                    </div>

                    <div class="localidad">
                        <label for="localidad">Localidad (*)</label>
                        <select name="localidad" id="localidad" required>
                            <option value="Las Palmas de Gran Canaria">Las Palmas de Gran Canaria</option>
                        </select>
                    </div>

                    <div class="codigoPostal">
                        <label for="codigoPostal">Código postal</label>
                        <input type="text" name="codigoPostal" id="codigoPostal" value='<?= isset($_POST["codigoPostal"])?$_POST["codigoPostal"]:""; ?>' />
                    </div>
                </div>

                <!-- // # MAS DATOS ################################################################################### -->
                <span class="separadores">MAS DATOS</span>
                <div class="container_masDatos">
                    <input type="radio" name="masDatos" value="Huerfano"> El alumno o alumna es huerfano
                    absoluto.</input>
                    <br />
                    <input type="radio" name="masDatos" value="Tutela"> El alumno se encuentra en régimen de tutela y
                    guarda por la Administración.</input>
                </div>

                <!-- // # ALERGIAS, PATOLOGIAS O DIETAS ESPECIALES ################################################################################### -->
                <span class="separadores">ALERGIAS, PATOLOGIAS O DIETAS ESPECIALES</span>
                <div class="container_alergias">
                    <textarea name="textarea" id="textarea" value='<?= isset($_POST["textarea"])?$_POST["textarea"]:""; ?>'></textarea>
                </div>

                <!-- // # DATOS ACADEMICOS ################################################################################### -->
                <span class="separadores">DATOS ACADEMICOS DEL ALUMNADO O ALUMNADA</span>
                <div class="container_datosAcademicos">
                    <p>Seleccione opción (seleccionar 1)</p>
                    <input type="radio" name="itinerario" id="salud" value="salud" required> ITINERARIO: CIENCIAS DE LA
                    SALUD</input>
                    <br />
                    <input type="radio" name="itinerario" id="tecnologico" value="tecnologico" required> ITINERARIO:
                    CIENTÍFICO-TECNOLÓGICO</input>
                    <details>
                        <summary>Bloque 1 (seleccionar 6)(máximo:6) y ordenar por preferencia</summary>
                        <input type="checkbox" name="lengua" id="lengua" value="Lengua Castellana y Literatura I"
                            checked onclick="return false"> Lengua Castellana y Literatura I</input>
                        <br />
                        <input type="checkbox" name="filosofia" id="filosofia" value="Filosofia" checked
                            onclick="return false"> Filosofía</input>
                        <br />
                        <input type="checkbox" name="edFisica" id="edFisica" value="Educacio Física" checked
                            onclick="return false"> Educación Física</input>
                        <br />
                        <input type="checkbox" name="matematicas" id="matematicas" value="Matematicas" checked
                            onclick="return false"> Matemáticas I</input>
                        <br />
                        <input type="checkbox" name="fisicayquimica" id="fisicayquimica" value="Fisica y Quimica"
                            checked onclick="return false"> Física y Química</input>
                        <br />
                        <input type="checkbox" name="tutoria" id="tutoria" value="Tutoria" checked
                            onclick="return false"> Tutoría</input>
                    </details>
                    <details>
                        <summary>Bloque 2 (seleccionar1)</summary>
                        <input type="radio" name="lenguaOp1" id="lenguaOp1" value="Ingles" required> Primera Lengua Extranjera
                        (Inglés) I </input>
                        <br />
                        <input type="radio" name="lenguaOp1" id="lenguaOp1" value="Italiano" required> Primera Lengua Extranjera
                        (Italiano) I</input>
                    </details>
                    <details>
                        <summary>Bloque 3 (seleccionar1)</summary>
                        <input type="radio" name="opcionales1" id="opcionales1" value="Biologia y Geologia" required>Biología y
                        Geología</input>
                        <br />
                        <input type="radio" name="opcionales1" id="opcionales1" value="Dibujo Tecnico" required> Dibujo
                        Técnico</input>
                    </details>
                    <details>
                        <summary>Bloque 4 (seleccionar1)</summary>
                        <input type="radio" name="opcionales2" id="opcionale2" value="Tecnologia Industrial I" required>
                        Tecnología Industrial I</input>
                        <br />
                        <input type="radio" name="opcionales2" id="opcionale2" value="Cultura Cientifica" required> Cultura
                        Científica</input>
                        <br />
                        <input type="radio" name="opcionales2" id="opcionale2"
                            value="Segunda Lengua Extranjera (Ingles) I" required> Segunda Lengua Extranjera (Inglés) I</input>
                        <br />
                        <input type="radio" name="opcionales2" id="opcionale2" value="Biologia y Geologia (E)" required> Biología
                        y Geología (E)</input>
                        <br />
                        <input type="radio" name="opcionales2" id="opcionale2" value="Dibujo Tecnico I (E)" required> Dibujo
                        Técnico I (E)</input>
                    </details>
                    <details>
                        <summary>Bloque 5 (seleccionar1)</summary>
                        <input type="radio" name="opcionales3" id="opcionales3" value="Religion Catolica" required> Religión
                        Católica</input>
                        <br />
                        <input type="radio" name="opcionales3" id="opcionales3"
                            value="Tecnologia de la informacion y la comunicacion I" required> Tecnología de la información y la
                        comunicación I</input>
                    </details>
                </div>

                <!-- // # MENOS DIFUSION ################################################################################### -->
                <span class="separadores">MEDIOS DE DIFUSIÓN</span>
                <div class="container_mediosDifusion">
                    <p><b>CONSENTIMIENTO INFORMADO TRATAMIENTO DE IMÁGENES/VOZ DEL ALUMNADO EN CENTROS DOCENTES DE
                            TITULARIDAD PUBLICA DE LA CONSEJERÍA DE EDUCACIÓN, UNIVERSIDADES, CULTURA Y DEPORTES</b></p>
                    <br />
                    <p>De acuerdo con el reglamento General de Protección de datos y la Ley Orgánica 3/2018, de 5 de
                        diciembre, de Protección e Datos Personales y Garantía de los Derechos Digitales, mediante la
                        firma del presente documento se presta voluntariamente el consentimiento inequívoco e informado
                        y se autoriza expresamente al centro docente al "tratamiento de la imagen / voz de actividades d
                        los centros de titularidad publica", mediante los siguientes medios solo se entenderá que
                        consiente la difusión de imágenes / voz por los medios expresamente marcados a continuación.</p>
                    <br />
                    <input type="radio" name="consentimiento1" id="consiente1" value="Conciente">
                    <label for="consentimiento1">Consiente</label>
                    <input type="radio" name="consentimiento1" id="noconsiente1" value="No Consiente">
                    <label for="consentimiento1">No consiente</label>
                    <br />
                    <br />
                    <div class="contenido">
                        <div class="contenido-izq">
                            <p>Página web del centro docente</p>
                        </div>
                        <div class="contenido-der">
                            <input type="radio" name="consentimiento2" id="consiente2" value="Conciente">
                            <label for="consentimiento2">Consiente</label>
                            <input type="radio" name="consentimiento2" id="noconsiente2" value="No Consiente">
                            <label for="consentimiento2">No Consiente</label>
                        </div>
                    </div>
                    <br />
                    <div class="contenido">
                        <div class="contenido-izq">
                            <p>App de los alumnos y familias</p>
                        </div>
                        <div class="contenido-der">
                            <input type="radio" name="consentimiento3" id="consiente3" value="Conciente">
                            <label for="consentimiento3">Consiente</label>
                            <input type="radio" name="consentimiento3" id="noconsiente3" value="No Consiente">
                            <label for="consentimiento3">No Consiente</label>
                        </div>
                    </div>
                    <br />
                    <div class="contenido">
                        <div class="contenido-izq">
                            <p>Facebook</p>
                        </div>
                        <div class="contenido-der">
                            <input type="radio" name="consentimiento4" id="consiente4" value="Conciente">
                            <label for="consentimiento4">Consiente</label>
                            <input type="radio" name="consentimiento4" id="noconsiente4" value="No Consiente">
                            <label for="consentimiento4">No Consiente</label>
                        </div>
                    </div>
                    <br />
                    <p><b>El consentimiento aquí otorgado podrá ser revocado en cualquier momento ante el propio centro
                            docente, teniendo en cuenta que dicha revocación no surtirá efectos retroactivos.</b></p>
                </div>

                <!-- // # DOCUMENTOS ADJUNTADOS ################################################################################### -->
                <span class="separadores">Documentos Adjuntos</span>
                <div class="container_documentosAdjuntos">
                    <div class="aviso">
                        <p><img src="themes/warning_black_24dp.svg">
                            <b> Aviso:</b>
                        </p>
                        <ul>
                            <li>Los formatos permitidos son jpg, png, txt, odt, pdf, jpeg, doc, docx</li>
                            <li>El tamaño máximo por fichero es de 10MB</li>
                            <li>El nombre de los ficheros no debe incluir caracteres acentuados, caracteres con
                                dieresis,
                                la eñe o caracteres especiales: !"#$%&'*,./;<=>?@[]()[]^`{|}</li>
                        </ul>
                    </div>
                </div>

                <!-- // # LISTA DE DOCUMENTOS PENDIENTES ################################################################################### -->
                <span class="separadores ama">Lista de documentos pendientes</span>
                <div class="container_documentosPendientes">
                    <hr />
                    <div>
                        <p>Documento</p>
                        <p>Acciones</p>
                    </div>
                    <hr />
                    <div class="contenido">
                        <div class="contenido-izq">
                            <p>DNI del alumno o alumna ( o de las padres, madres o tutores legales de alumnado sin
                                DNI)(SOLO ALUMNADO NUEVO)</p>
                        </div>
                        <div class="contenido-der">
                            <input type="file" class="file" name="archivo1">
                        </div>
                    </div>

                    <div class="contenido">
                        <div class="contenido-izq">
                            <p>Para el alumnado procedente de otros centros, certificación académica
                                del centro de origen en el que se especifique la promoción de curso o la terminación de
                                estudios con propuestas para titulación</p>
                        </div>
                        <div class="contenido-der">
                            <input type="file" class="file" name="archivo2">
                        </div>
                    </div>

                </div>
                <!-- // # BOTONES ################################################################################### -->
                <div class="container_botones">
                    <button type="submit" name="procesar">PROCESAR</button>
                    <button type="reset" name="reset">CANCELAR</button>
                </div>

            </form>
        </div>
    </body>

</html>

<!-- https://code.tutsplus.com/es/tutorials/easy-form-validation-with-jquery--cms-33096 -->