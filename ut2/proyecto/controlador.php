<?php
    include "datos/datos.php";
    include "funciones.php";

    $patron_numero = "/^([0-9])+$/";
    $patron_codigoPostal = "/^(?:0?[1-9]|[1-4]\d|5[0-2])\d{3}$/";
    $patron_dni = "/^([0-9]){8}+([a-zA-Z]){1}+$/";
    $patron_texto = "/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ{8}\s]+$/";
    $patron_correo = "/^([a-z0-9_.-])+@([a-z0-9_.-])+\.([a-z]){2,4}+$/";

    if(isset($_POST["procesar"])){
        // - array = validarStringsOblig(array, name, patron, minlength, maxlength, mensaje error);
        // - array = validarEnterosOblig(array, name, patron, minlength, maxlength, mensaje error);
        // - array = validarSelectsOblig(array, name);
        // Actua como representante
        $datos = validarStringsOblig($datos, "representante", $patron_texto, 5, 25, "Valor Representante no cumple el formato");
        // # DATOS DEL REPRESENTANTE ###################################################################################
        // * Valores Obligatorios
        $datos = validarSelectsOblig($datos, "documento");
        $datos = validarOtrosOblig($datos, "numIdentificacion", $patron_dni, "DNI no valido");
        $datos = validarOtrosOblig($datos, "correo", $patron_correo, "Correo no valido");
        $datos = validarStringsOblig($datos, "nombre", $patron_texto, 3, 8, "Nombre no cumple con el formato");
        $datos = validarStringsOblig($datos, "primerApellido", $patron_texto, 3, 10, "Primer Apellido no cumple el formato solicitado");
        $datos = validarStringsOblig($datos, "segundoApellido", $patron_texto, 3, 10, "Segundo Apellido no cumple el formato solicitado");
        $datos = validarEnterosOblig($datos, "movil", $patron_numero, 9, 9, "Un movil tiene 9 digitos. Ej: 123 45 67 89");
        $datos = validarSelectsOblig($datos, "calidad");
        // * Valores no Obligatorios
        $datos = validarEnterosNOblig($datos, "telefono", $patron_numero, 9, 9);

        // # DOMICILIO DE CONTACTO ###################################################################################
        // * Valores Obligatorios
        $datos = validarSelectsOblig($datos, "tipoVia");
        $datos = validarStringsOblig($datos, "nombreVia", $patron_texto, 4, 40, "Nombre de via no valido");
        $datos = validarEnterosOblig($datos, "numero", $patron_numero, 1, 2, "Numero no valido");
        $datos = validarSelectsOblig($datos, "pais");
        $datos = validarSelectsOblig($datos, "provincia");
        $datos = validarSelectsOblig($datos, "municipio");
        $datos = validarSelectsOblig($datos, "isla");
        $datos = validarSelectsOblig($datos, "localidad");
        $datos = validarEnterosOblig($datos, "codigoPostal", $patron_codigoPostal, 5, 5, "Codigo postal no valido");
        // * Valores no Obligatorios
        $datos = validarEnterosNOblig($datos, "bloque", $patron_numero, 1, 2);
        $datos = validarEnterosNOblig($datos, "escalera", $patron_numero, 1, 2);
        $datos = validarEnterosNOblig($datos, "piso", $patron_numero, 1, 2);
        $datos = validarEnterosNOblig($datos, "portal", $patron_numero, 1, 2);
        $datos = validarStringsNOblig($datos, "letra", $patron_texto, 1, 1);
        $datos = validarEnterosNOblig($datos, "puerta", $patron_numero, 1, 2);

        // # ALERGIAS, PATOLOGIAS O DIETAS ESPECIALES ###################################################################################
        // * Valores Obligatorios
        $datos = validarSelectsOblig($datos, "itinerario");
        $datos = validarSelectsOblig($datos, "lenguaOp1");
        $datos = validarSelectsOblig($datos, "opcionales1");
        $datos = validarSelectsOblig($datos, "opcionales2");
        $datos = validarSelectsOblig($datos, "opcionales3");

        // # ALERGIAS, PATOLOGIAS O DIETAS ESPECIALES ###################################################################################
        $datos = validarStringsNOblig($datos, "textarea", $patron_texto, 0,500);

        //consentimiento
        $datos = validarStringsOblig($datos, "consentimiento1", $patron_texto, 5, 25, "Valor Consentimiento no cumple el formato");
        $datos = validarStringsOblig($datos, "consentimiento2", $patron_texto, 5, 25, "Valor Consentimiento Web del Centro no cumple el formato");
        $datos = validarStringsOblig($datos, "consentimiento3", $patron_texto, 5, 25, "Valor Consentimiento App no cumple el formato");
        $datos = validarStringsOblig($datos, "consentimiento4", $patron_texto, 5, 25, "Valor Consentimiento Facebook no cumple el formato");

        // ! Subida de archivos
        $arch1 = $_FILES['archivo1'];
        $arch2 = $_FILES['archivo2'];
        subirArchivo($arch1, $datos);
        subirArchivo($arch2, $datos);

        if( count($datos["error"]) ){
            echo '<div class="errores">';
                echo "<ul>";
                    foreach($datos["error"] as $errores){
                        echo "<li>".$errores."</li>";
                    }
                echo "</ul>";
            echo "</div>";
        }else{
            generarSolicitud($datos["datos"]);
        }
    }
?>