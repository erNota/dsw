
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Estadisticas del texto</title>
    <script src="js/chart.min.js"></script>

    <style>
        div {
            border: 2px solid red;
        }
    </style>

</head>
<body>

    <h2>Estadisticas del texto</h2>

    <div style="width: 45%; height: 400px; float:left">
        <canvas id="myChart"></canvas>   
    </div>

    
    <div style="width: 45%; height: 400px; float:right">
        <canvas id="grafica-2"></canvas>   
    </div>

    <script>
        var datos = {
            labels: ["Acierto 1", "Acierto 2", "Acierto 3", "Acierto 4"],
            datasets: [{
                label: '# de intentos',
                data: [<?= $datos[0] ?>,<?= $datos[1] ?>,<?= $datos[2] ?>, <?= $datos[3] ?>],
                backgroundColor: [
                    'rgba(255, 99, 132)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                ]
            }]
        };
    
        var opciones =  { maintainAspectRatio: false }

        var ctx = document.getElementById('myChart').getContext('2d');
        var grafica = new Chart(ctx, { type: 'bar', data: datos, options: opciones });

        var ctx2 = document.getElementById('grafica-2').getContext('2d');
        var grafica2 = new Chart(ctx2, { type: 'doughnut', data: datos, options: opciones });

        
    </script>
</body>
</html>