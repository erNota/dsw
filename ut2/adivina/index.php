<?php
    // Genera numero entre 1 y 10000
    $limite = 10000;
    $tiempoMaximo = 3;
    $numero = rand(1, $limite);

    // PHP_INT_MAX --> MAYOR N ENTERO

    // formatear numeros 
    // echo number_format(PHP_INT_MAX);

    echo "<br>";

    // tiempo en sg
    $inicio = microtime(true);

    $numAciertos = 4;
    $aciertos = 0;
    $intentos = 0;
    $datos = []; // Arrays $datos = array()

    // Mostrar intentos por acierto 
    while ($aciertos < $numAciertos) {
        $ordenador = rand(1, $limite);
        $intentos++;

        if ($numero == $ordenador) {
            $datos[] = $intentos; // Guardar en la ultima posicio del array el ultimo intento;

            $aciertos++;
            echo "Acierto nº ".$aciertos." nº intentos ".number_format($intentos)."<br>";
        
            $intentos = 0;
        }

        // Controlar tiempo.
        if ( (microtime(true) - $inicio) > $tiempoMaximo ) {
            echo "Tiempo maximo excedido ($tiempoMaximo sg)";
            break;
        }
    }

    $fin = microtime(true);
    $tarda = $fin - $inicio;

    echo "Ha tardado ".$tarda. "sg";

    //print_r ($datos);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Estadisticas del texto</title>

    <script src="js/chart.min.js"></script>

    <style>
        div {
            border: 2px solid red;
        }
    </style>

</head>
<body>

<h2>Estadisticas del texto</h2>

<div style="width: 45%; height: 400px; float:left">
    <canvas id="myChart"></canvas>
</div>


<script>
    var datos = {
        labels: ["Acierto 1", "Acierto 2", "Acierto 3", "Acierto 4"],
        datasets: [{
            label: '# de intentos',
            data: [1,5,6,8],
            backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
            ]
        }]
    };

    var opciones =  { maintainAspectRatio: false }
    var ctx = document.getElementById('myChart').getContext('2d');
    var grafica = new Chart(ctx, { type: 'bar', data: datos, options: opciones });

</script>
</body>
</html>