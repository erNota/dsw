<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IMC</title>

    <style>
        .rojo {
            color: red;
        }
    </style>
</head>

<body>

    <?php
        $imc = 0;
        $descripcion = "";
        $peso = 0;
        $altura = 0;

        if ( isset( $_GET["calcular"]) ) {
            $peso = $_GET["peso"];
            $altura = $_GET["altura"];

            $imc = $peso / ( $altura * $altura);
            $imc = round ($imc, 3);

            if ($imc < 18.5) {
                $descripcion = '<span class="rojo">Bajo peso</span>';
            } else if ($imc < 24.9 ) {
                $descripcion = "Peso normal";
            } else if ($imc < 29.9 ) {
                $descripcion = "Sobrepeso";
            } else {
                $descripcion = "Obesidad";
            }
        }
    ?>

    <form action="imc.php" method="GET">
        <div>
            <select name="sexo">
                <option value="0">Femenino</option>
                <option value="1">Masculino</option>
            </select>
        </div>

        <div>
            <label for="peso">Peso</label>
            <input type="text" name="peso" value="<?= $peso ?>">
        </div>

        <div>
            <label for="altura">Altura</label>
            <input type="text" name="altura" value="<?= $altura ?>">
        </div>

        <div>
            IMC: <?php echo $imc; ?>

            <div>
                <?php echo $descripcion; ?>
            </div>

        </div>

        <button type="submit" name="calcular">Calcular</button>
        <button type="reset">Borrar</button>

    </form>

</body>

</html>