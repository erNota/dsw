<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="author" content="Agustin">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Space+Grotesk&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Horoscopo</title>
</head>
<?php
$horoscopos = [
    'Acuario' => ["descripcion" => "Camus de Acuario", "url" => "<img src=\"themes/acuario.png\" "],
    'Piscis' => ["descripcion" => "Afrodita de Piscis", "url" => "<img src=\"themes/piscis.png\" "],
    'Aries' => ["descripcion" => "Mu de Aries", "url" => "<img src=\"themes/aries.png\" "],
    'Tauro' => ["descripcion" => "Aldebaran de Tauro", "url" => "<img src=\"themes/tauro.png\" "],
    'Geminis' => ["descripcion" => "Saga de Geminis", "url" => "<img src=\"themes/geminis.png\" "],
    'Cancer' => ["descripcion" => "Deathmask de Cancer", "url" => "<img src=\"themes/cancer.png\" "],
    'Leo' => ["descripcion" => "Aiolos de Sagitario", "url" => "<img src=\"themes/leo.png\" "],
    'Virgo' => ["descripcion" => "Shaka de Virgo", "url" => "<img src=\"themes/virgo.png\" "],
    'Libra' => ["descripcion" => "Dohko de Libra", "url" => "<img src=\"themes/libra.png\" "],
    'Escorpio' => ["descripcion" => "Milo de Escorpion", "url" => "<img src=\"themes/escorpio.png\" "],
    'Sagitaro' => ["descripcion" => "Aiolos de Sagitario", "url" => "<img src=\"themes/sagitario.png\" "],
    'Capricornio' => ["descripcion" => "shura de Capricornio", "url" => "<img src=\"themes/capricornio.png\" "]
];
?>

<body class="fondo">
    <div class="container">
        <div>
            <h1>CABALLEROS DEL ZODIACO</h1>
        </div>
        <div>
            <form action=" <?= $_SERVER["PHP_SELF"] ?>" method="POST">
                <?php
                $fecha = 0;
                $resultado = "Acuario";
                ?>
                <div>
                    <label>Introducir Fecha:</label>
                    <input type="date" name="fecha" value="<?= $fecha ?>">
                </div>

                <div>
                    <button type="submit" class="btn btn-light">Enviar</button>
                </div>

            </form>
        </div>
    </div>

    <?php

    if (isset($_REQUEST["fecha"])) {
        $fecha = $_POST["fecha"];
        if ($fecha != null) {
            $resultado = validar($fecha);
        } else {
            echo "El valor fecha esta vacio";
        }
    }




    function validar($fecha)
    {
        $resultado = "Acuario";
        $valores = explode('-', $fecha);
        // echo (count($valores).'-');
        // echo $valores[1].'-';
        // echo $valores[2];
        // && checkdate($valores[0], $valores[1], $valores[2])
        if (count($valores) == 3) {
            // echo $valores[1];
            // echo $valores[2];
            if (($valores[1] == 1 && $valores[2] >= 20) || ($valores[1] == 2 && $valores[2] <= 18)) {
                $resultado = "Acuario";
            } else if (($valores[1] == 2 && $valores[2] >= 19) || ($valores[1] == 3 && $valores[2] <= 20)) {
                $resultado = "Piscis";
            } else if (($valores[1] == 3 && $valores[2] >= 21) || ($valores[1] == 4 && $valores[2] <= 19)) {
                $resultado = "Aries";
            } else if (($valores[1] == 4 && $valores[2] >= 20) || ($valores[1] == 5 && $valores[2] <= 21)) {
                $resultado = "Tauro";
            } else if (($valores[1] == 5 && $valores[2] > 21) || ($valores[1] == 6 && $valores[2] <= 20)) {
                $resultado = "Geminis";
            } else if (($valores[1] == 6 && $valores[2] >= 21) || ($valores[1] == 7 && $valores[2] <= 22)) {
                $resultado = "Cancer";
            } else if (($valores[1] == 7 && $valores[2] >= 23) || ($valores[1] == 8 && $valores[2] <= 22)) {
                $resultado = "Leo";
            } else if (($valores[1] == 8 && $valores[2] >= 24) || ($valores[1] == 9 && $valores[2] <= 22)) {
                $resultado = "Virgo";
            } else if (($valores[1] == 9 && $valores[2] >= 23) || ($valores[1] == 10 && $valores[2] <= 22)) {
                $resultado = "Libra";
            } else if (($valores[1] == 10 && $valores[2] >= 23) || ($valores[1] == 11 && $valores[2] <= 21)) {
                $resultado = "Escorpio";
            } else if (($valores[1] == 11 && $valores[2] >= 22) || ($valores[1] == 12 && $valores[2] <= 21)) {
                $resultado = "Sagitario";
            } else {
                $resultado = "Capricornio";
            }
        }
        return $resultado;
    }
    ?>

    <div class="container2">

        <div class="borde">
            <div>
                <h3>
                    <?= $horoscopos[$resultado]["descripcion"]; ?>
                </h3>
            </div>

            <div>
                <?= $horoscopos[$resultado]["url"] ?>
            </div>
        </div>
    </div>

</body>

</html>