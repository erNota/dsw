<?php

$aciertos = 0;
$errores = 0;
$porcentaje = "0%";

?>

<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="author" content="agustin">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Secular+One&display=swap" rel="stylesheet">
    <title>Test de autoescuela</title>
</head>

    <body>

        <?php include "datos/test1.php"; ?>

        <h1> 심화운전학원 AUTOESCUELA A FONDO</h1>

        <form action="<?= $_SERVER["PHP_SELF"] ?>" METHOD="post">
            <div class="container">
                <?php
                foreach ($preguntas as $pregunta) {
                    $numero = $pregunta["num"];

                    echo "<div class='pregunta'>";

                        // titulo pregunta
                        echo "<div>";
                            echo "<span>" . $pregunta["num"] . "</span> ";
                            echo $pregunta["pregunta"];

                            // Opciones
                            echo "<ul>";
                            foreach ($pregunta["opciones"] as $clave => $opcion) {
                                $checked = (isset($_POST["respuesta_" . $numero]) &&
                                    $_POST["respuesta_" . $numero] == $clave) ? "checked" : "";

                                echo "<li class='opcion'>";
                                echo "<input $checked type='radio' name='respuesta_" . $numero . "' value='" . $clave . "'>\n";
                                echo $opcion;
                                echo "</li>";
                            }
                            echo "</ul>";
                        echo "</div>";

                        // imagen
                        echo "<div>";
                            if($pregunta["imagen"]==""){
                            }else{
                                echo "<img class=\"imagen\" src=\"imagenes/" . $pregunta["imagen"] . "\">";
                            }
                            

                            if (isset($_POST["respuesta_" . $numero])) {
                                if ($pregunta["respuesta"] != $_POST["respuesta_" . $numero]) {
                                    echo "<p class=\"error\">Respuesta incorrecta. ";
                                    echo "<span class=\"correcta\">La respuesta \"correcta\" es " . $pregunta["opciones"][$pregunta["respuesta"]] . "</span>";
                                    echo "</p>";
                                    $errores++;
                                } else {
                                    echo "<p class=\"ok\">Respuesta correcta</p>";
                                    $aciertos++;
                                }
                            }
                        echo "</div>";

                    echo "</div>";
                }
                ?>
                <div>
                    <button type="submit" value="Comprobar" name="Comprobar">Comprobar</button>
                    <input type="text" value="Errores: <?= $errores ?>" readonly>
                    <input type="text" value="Aciertos: <?= $aciertos ?>" readonly>
                    <input type="text" value="Porcentaje Aciert: <?=  $porcentaje = (($aciertos/count($preguntas))*100)?>%" readonly>
                </div>
            </div>

        </form>
    </body>
</html>