<?php
    $preguntas = [
    ["num" => "1",
    "pregunta" =>"¿El alcohol influye en la distancia de detención?",
    "opciones" => [
    "a) Sí, disminuyéndola.",
    "b) Sí, aumentándola.",
    "c) No, la distancia de detención sólo depende de la velocidad."
    ],
    "respuesta" => 0,
    "imagen" => "p1.jpg",
    "pista" => ""
    ],

    ["num" => "2",
    "pregunta" =>"Circulando por una vía frecuentada por peatones, especialmente niños o ancianos, ¿qué haremos?",
    "opciones" => [
    "a) Reducir la velocidad, incluso llegando a detenerme.",
    "b) Adoptaré las medidas necesarias para su seguridad, sin tener que moderar obligatoriamente la velocidad."
    ],
    "respuesta" => 1,
    "imagen" => "p2.jpg",
    "pista" => ""
    ],

    ["num" => "3",
    "pregunta" =>"Circulando por una vía frecuentada por peatones, especialmente niños o ancianos, ¿qué haremos?",
    "opciones" => [
    "a) Reducir la velocidad, incluso llegando a detenerme.",
    "b) Adoptaré las medidas necesarias para su seguridad, sin tener que moderar obligatoriamente la velocidad."
    ],
    "respuesta" => 1,
    "imagen" => "p3.jpg",
    "pista" => ""
    ],

    ["num" => "4",
    "pregunta" =>"¿El alcohol influye en la distancia de detención?",
    "opciones" => [
    "a) Sí, disminuyéndola.",
    "b) Sí, aumentándola.",
    "c) No, la distancia de detención sólo depende de la velocidad."
    ],
    "respuesta" => 0,
    "imagen" => "p4.jpg",
    "pista" => ""
    ]

    ];
?>
