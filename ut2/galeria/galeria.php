<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width" />

    <!-- libreria para galeria de imagenes -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
  
    <style>
      img {
        height: 200px;
      }
    </style>
  
  
  </head>

  <body>
    <h1>Galeria de imagenes (Libreria FancyBox)</h1>
    
    <?php

      $ruta = "imagenes/*.jpg";

      // Obtener las imagenes de una carpeta
      $imagenes = glob($ruta); 
      $i=0;

      // Recorrer los elementos de un array.
      foreach ($imagenes as $indice => $imagen) { ?>
        <a data-fancybox="galeria" href="<?= $imagen ?>">
          <img src="<?= $imagen ?>" title='Imagen <?= $indice ?>'>
        </a>
      <?php } ?>

    ?>

  </body>
</html>