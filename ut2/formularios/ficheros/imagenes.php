<h1>Subir ficheros</h1>

<form enctype="multipart/form-data" action="<?= $_SERVER["PHP_SELF"] ?>" method="POST">
    Enviar este fichero: <input name="fichero_usuario" type="file" />
    <input type="submit" name="enviar" value="Enviar fichero" />
</form>

<?php

if (isset($_POST["enviar"])) {
    $dir_subida = 'imagenes/'; // donde subo los archivos
    $fichero_subido = $dir_subida . basename($_FILES['fichero_usuario']['name']);
    //control de subida de archivo
    $valor = explode(".", $fichero_subido);
    if ($valor[1] == "jpg" || $valor[1] == "png" || $valor[1] == "webp") {
        echo '<pre>';
        if (move_uploaded_file($_FILES['fichero_usuario']['tmp_name'], $fichero_subido)) {
            echo "El fichero es válido y se subió con éxito.\n";
        } else {
            echo "¡Posible ataque de subida de ficheros!\n";
        }

        echo 'Más información de depuración:';
        print_r($_FILES);

        print "</pre>";
    } else {
        echo "Extension no permitida " .$valor[1];
    }
}
?>

<h2>Listado de imagenes</h2>

<?php

$imagenes = glob("imagenes/*.*");

foreach ($imagenes as $imagen) {
    echo "<img src=\"$imagen\">";
}

//https://github.com/alacritty/alacritty

?>