<?php
    $preguntas = [
        ["num" => "1",
            "pregunta" =>"¿El alcohol influye en la distancia de detención?",
            "opciones" => [
                "a) Sí, disminuyéndola.",
                "b) Sí, aumentándola.",
                "c) No, la distancia de detención sólo depende de la velocidad."
            ],
            "respuesta" => 0,
            "imagen" => "",
            "pista" => ""
        ],

        ["num" => "2",
            "pregunta" =>"Circulando por una vía frecuentada por peatones, especialmente niños o ancianos, ¿qué haremos?",
            "opciones" => [
                "a) Reducir la velocidad, incluso llegando a detenerme.",
                "b) Adoptaré las medidas necesarias para su seguridad, sin tener que moderar obligatoriamente la velocidad."
            ],
            "respuesta" => 1,
            "imagen" => "p2.jpg",
            "pista" => ""
        ],

        ["num" => "3",
            "pregunta" =>"Circulando por una vía frecuentada por peatones, especialmente niños o ancianos, ¿qué haremos?",
            "opciones" => [
                "a) Reducir la velocidad, incluso llegando a detenerme.",
                "b) Adoptaré las medidas necesarias para su seguridad, sin tener que moderar obligatoriamente la velocidad."
            ],
            "respuesta" => 1,
            "imagen" => "p3.jpg",
            "pista" => ""
        ],

        ["num" => "4",
            "pregunta" =>"¿El alcohol influye en la distancia de detención?",
            "opciones" => [
                "a) Sí, disminuyéndola.",
                "b) Sí, aumentándola.",
                "c) No, la distancia de detención sólo depende de la velocidad."
            ],
            "respuesta" => 0,
            "imagen" => "",
            "pista" => ""
        ]

    ];
?>

<style>
    div {
        margin: 1em;
        border: 2px dotted red;
    }
</style>

<?php

    if (isset($_POST["comprobar"])) {
        print_r($_POST);

        $respuestas = $_POST;

        echo "La respuesta 1 es " . $respuestas["respuesta_1"] . "<br>";
        echo "La respuesta 2 es " . $respuestas["respuesta_2"] . "<br>";
    }
?>


<form action="<?= $_SERVER["PHP_SELF"] ?>" METHOD="post">
    <?php

    
        foreach ($preguntas as $pregunta) {
                $numero = $pregunta["num"];
    
                echo "<div class='pregunta'>";
                    echo "<span>".$pregunta["num"]."</span> ";
                    echo $pregunta["pregunta"];
    
                    echo "<ul>";
                    foreach ($pregunta["opciones"] as $clave=>$opcion) {
                        echo "<li class='opcion'>";
                        echo "<input type='radio' name='respuesta_".$numero."' value='".$clave."'>\n";
                        echo $opcion;
                        echo "</li>";
                    }
                    echo "</ul>";
    
                    //echo "Tu es respuesta: ".
    
                echo "</div>";
            }  
    
        
    ?>



    <input type="submit" value="Comprobar" name="comprobar">
</form>