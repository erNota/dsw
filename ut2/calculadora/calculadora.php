<!DOCTYPE html>
<html lang="es">
    <head>

        <!-- 
            Autor: Agustín Domingo Déniz Quintana
            Ejercicio: Calculadora
        -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="themes/calculator.webp">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Orbitron&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <title>CALCULADORA</title>
    </head>
    <body>
        <header class="container-lg">
            <h1>CALCULADORA</h1>
        </header>
        <form method="POST">
            <?php  
                    $number1 = 0;
                    $number2 = 0;
                    $resultado = 0;
                    //var_dump($_GET);
                    if(isset($_REQUEST["number1"]) && isset($_REQUEST["number2"])){
                        $number1 = $_REQUEST["number1"];
                        $number2 = $_REQUEST["number2"];
                        if(empty($number1) && empty($number2)){
                            $number1 = empty($_REQUEST["number1"]) ? "0" : $_REQUEST["number1"];
                            $number2 = empty($_REQUEST["number2"]) ? "0" : $_REQUEST["number2"]; 
                            $error = "Los valores estan vacios";
                        }else if(empty($number2)){
                            $number2 = empty($_REQUEST["number2"]) ? "0" : $_REQUEST["number2"];
                            if($_REQUEST["operacion"]=='+'){
                                $resultado = $number1 + $number2;
                            }else if($_REQUEST["operacion"]=='-'){
                                $resultado = $number1 - $number2;
                            }else if($_REQUEST["operacion"]=='*'){
                                $resultado = $number1 * $number2;
                            }else if($_REQUEST["operacion"]=='/'){
                                if($number2 == 0){
                                    $resultado = "Syntax Error";
                                }else{
                                    $resultado = $number1 / $number2;
                                }
                            }else if($_REQUEST["operacion"]=='^y'){
                                $resultado = $number1 ** $number2;
                            }else if($_REQUEST["operacion"]=='%'){
                                $resultado = $number1 % $number2;
                            }else{
                                $resultado = "Error";
                            }
                        }else if(empty($number1)){
                            $number1 = empty($_REQUEST["number1"]) ? "0" : $_REQUEST["number1"];
                            if($_REQUEST["operacion"]=='+'){
                                $resultado = $number1 + $number2;
                            }else if($_REQUEST["operacion"]=='-'){
                                $resultado = $number1 - $number2;
                            }else if($_REQUEST["operacion"]=='*'){
                                $resultado = $number1 * $number2;
                            }else if($_REQUEST["operacion"]=='/'){
                                if($number2 == 0){
                                    $resultado = "Syntax Error";
                                }else{
                                    $resultado = $number1 / $number2;
                                }
                            }else if($_REQUEST["operacion"]=='^y'){
                                $resultado = $number1 ** $number2;
                            }else if($_REQUEST["operacion"]=='%'){
                                $resultado = $number1 % $number2;
                            }else{
                                $resultado = "Error";
                            }
                        }else{
                            if($_REQUEST["operacion"]=='+'){
                                $resultado = $number1 + $number2;
                            }else if($_REQUEST["operacion"]=='-'){
                                $resultado = $number1 - $number2;
                            }else if($_REQUEST["operacion"]=='*'){
                                $resultado = $number1 * $number2;
                            }else if($_REQUEST["operacion"]=='/'){
                                if($number2 == 0){
                                    $resultado = "Syntax Error";
                                }else{
                                    $resultado = $number1 / $number2;
                                }
                            }else if($_REQUEST["operacion"]=='^y'){
                                $resultado = $number1 ** $number2;
                            }else if($_REQUEST["operacion"]=='%'){
                                $resultado = $number1 % $number2;
                            }else{
                                $resultado = "Error";
                            }
                        }     
                    }
                ?>
        <div class="container">
            <div>
                <input type="text" class="form-control-lg" name="number1" value=<?= $number1 ?> />
            </div>
            <div>
                <input type="text" class="form-control-lg" name="number2" value=<?= $number2 ?> />
            </div> 
            <div>
                <div>
                    <input type="text" style = "background-color:rgb(124, 58, 97,0.7)" class="form-control-lg" name="resultado" readonly value=<?= $resultado ?> />
                </div>
            </div>
            <div>
                <button type="submit" class="btn boton" name="operacion" value="+">+</button>
                <button type="submit" class="btn boton" name="operacion" value="-">-</button>
                <button type="submit" class="btn boton" name="operacion" value="*">*</button>
                <button type="submit" class="btn boton" name="operacion" value="/">/</button>
                <button type="submit" class="btn boton" name="operacion" value="^y">^y</button>
                <button type="submit" class="btn boton" name="operacion" value="%">%</button>
                <button type="button" onclick="location.href='./calculadora.php'"  class="btn btn-secondary">AC</button>
            </div>
        </div>
        </form>
    </body>
</html>