<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name=agustin" content="width=device-width, initial-scale=1.0">
        <title>Ejercicios</title>
        <link 
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" 
            rel="stylesheet" 
            integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" 
            crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <h1>Agustín</h1>
        <h2>Indice de actividades</h2>
        <h3>UT0</h3>
        <div class="container">
            <div>
                Max Verstappen: 
                <button class="max_verstappen btn btn-info"><a href="ut0/index.html">MaxVerstappen</a></button>
            </div>
        </div>

        <h3>UT2</h3>
        <div class="container">
            <div>
                Actividad 1:
                <button class="calculadora btn btn-info"><a href="ut2/calculadora/calculadora.php">Calculadora</a></button>
            </div>

            <div>
                Actividad 2:
                <button class="imc btn btn-info"><a href="ut2/imc/imc.php">IMC</a></button>
            </div>

            <div>
                Actividad 3:
                <button class="imc btn btn-info"><a href="ut2/horoscopo/index.php">Horoscopo</a></button>
            </div>
            
        </div>
        
    </body>
</html>